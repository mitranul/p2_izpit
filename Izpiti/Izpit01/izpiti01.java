import jdk.nashorn.internal.runtime.ECMAException;

import javax.naming.LimitExceededException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;

class LimitedHashSetFullError extends java.lang.Error {
    @Override
    public String getMessage() {
        return "Mnozica je presegla velikost 1024 elementov!";
    }
}

class LimitedHashSet extends java.util.HashSet {
    @Override
    public boolean add(Object o) {
        if(! super.contains(o) && super.size() > 1023)
            throw new LimitedHashSetFullError();
        return super.add(o);
    }
}


public class izpiti01 {

    static void povprecnaTemp(String imeDatoteke) throws Exception {
        try {
            java.util.Scanner sc = new java.util.Scanner(new java.io.File(imeDatoteke));
            TreeMap<String, List<Float>> slovarMest = new TreeMap<>();
            TreeMap<String, Float> slovarPovprecij = new TreeMap<>();

            while(sc.hasNextLine()) {
                String[] tabela = sc.nextLine().split(" ");
                List<Float> tabelaMeritev = new ArrayList<>();

                for(int i = 1; i < tabela.length; i++)
                    tabelaMeritev.add(Float.parseFloat(tabela[i].replace(",", ".")));
                slovarMest.put(tabela[0], tabelaMeritev);
            }
            sc.close();

            int velikostTabele = 12;
            float vsota = 0, letnaTemp, vs = 0;

            for(String mesto : slovarMest.keySet()) {
                for(float temp : slovarMest.get(mesto)) {
                    vsota += temp;
                }
                slovarPovprecij.put(mesto, vs);
                vs = 0;
            }
            letnaTemp = vsota / 12;

        }catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) throws Exception {
        // povprecnaTemp("meritve.txt");
        LimitedHashSet hash = new LimitedHashSet();
        HashSet<Integer> set = new HashSet<>();

        for(int i = 0; i < 1022; i++) {
            hash.add(i);
        }
        System.out.println(hash.size());
    }
}
