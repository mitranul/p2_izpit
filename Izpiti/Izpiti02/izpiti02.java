import jdk.jfr.events.ExceptionThrownEvent;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import sun.reflect.generics.tree.Tree;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.TreeMap;

class Ekipa {
    String ime;
    int prejetih, danih, stTekem, stTock;

    public Ekipa(String ime) {
        this.ime = ime;
    }

    public String toString() {
        return "";
    }
}

class Skupina {
    TreeMap<String, Ekipa> skupina = new TreeMap<>();
    String imeSkupine;

    public Skupina(String imeSkupine){
        this.imeSkupine = imeSkupine;
    }

    void dodajEkipo(Ekipa e) {
        if(this.skupina.size() - 1  == 4)
            System.out.println("Skupina je ze polna!");
        else
            this.skupina.put(e.ime, e);
    }

    void dodajTekmo(String ekipa1, int stGolov1, String ekipa2, int stGolov2) {
        skupina.get(ekipa1).danih += stGolov1;
        skupina.get(ekipa2).danih += stGolov2;

        skupina.get(ekipa1).prejetih += stGolov2;
        skupina.get(ekipa2).prejetih += stGolov1;

        skupina.get(ekipa1).stTekem++;
        skupina.get(ekipa2).stTekem++;

        if(stGolov1 == stGolov2){
            skupina.get(ekipa1).stTock++;
            skupina.get(ekipa2).stTock++;
        }
        else if(stGolov1 > stGolov2)
            skupina.get(ekipa1).stTock += 3;
        else
            skupina.get(ekipa2).stTock += 3;

    }

    @Override
    public String toString() {

        String temp = String.format("Skupina: " + this.imeSkupine +"\n"+
                "Reprezentanca            Št. tekem Goli Točke\n" +
                "--------------------------------------------------\n");
        for (String s : this.skupina.keySet()) {
            temp+=String.format("%-24s     %-5d %d:%d %4d\n", this.skupina.get(s).ime, this.skupina.get(s).stTekem, this.skupina.get(s).danih, this.skupina.get(s).prejetih, this.skupina.get(s).stTock);
        }
        return temp;
    }
}


public class izpiti02 {

    static void preberiMeritve(String imeDatoteke) throws Exception {
        try {
            java.util.Scanner sc = new java.util.Scanner(new java.io.File(imeDatoteke));
            TreeMap<String, List<Float>> slovar = new java.util.TreeMap<>();

            while(sc.hasNextLine()) {
                String[] vrstica = sc.nextLine().split(" ");
                String letnica = vrstica[0].substring(6, 10);

                if(slovar.containsKey(letnica)) {
                    List<Float> meritev = slovar.get(letnica);
                    meritev.add(Float.parseFloat(vrstica[1].replace(",", ".")));
                    slovar.put(letnica, meritev);
                }
                else {
                    java.util.List<Float> meritev = new java.util.ArrayList<>();
                    meritev.add(Float.parseFloat(vrstica[1].replace(",", ".")));
                    slovar.put(letnica, meritev);
                }
            }
            sc.close();

            float vsota = 0, prejsnja = 0;
            boolean prva = true;

            for(String letnica : slovar.keySet()) {
                List<Float> tabela = slovar.get(letnica);

                for(int i = 0; i < tabela.size(); i++) {
                    vsota += tabela.get(i);
                }
                float pvp = vsota / tabela.size();

                if(prva) {
                    System.out.println(letnica + ": " + pvp);
                    prva = false;
                }

                if(pvp >= 0)
                    System.out.println(letnica + ": " + pvp + " (" + (pvp - prejsnja) + ")");
                else
                    System.out.println(letnica + ": " + pvp + " (" + (pvp - prejsnja) + ")");
                vsota = 0;
                prejsnja = pvp;
            }

        }catch (Exception e) {
            System.out.println(e);
        }
    }

    static void genStavke(String[] t1, String[] t2, String[] t3) {
        for(int i = 0; i < t1.length; i++) {
            String prvaCrka = t1[i].substring(0,1).toUpperCase();
            for(int j = 0; j < t2.length; j++)
                for(int k = 0; k < t3.length; k++)
                    System.out.println(prvaCrka + t1[i].substring(1, t1[i].length()) + " " + t2[j] + " " + t3[k] + ".");
        }
    }





    public static void main(String[] args) throws Exception {
        /*Ekipa e = new Ekipa("Danska");
        Ekipa r = new Ekipa("Nizozemska");
        Ekipa b = new Ekipa("Portugalska");
        Ekipa c = new Ekipa("Nemčija");
        Skupina s = new Skupina("B");
        s.dodajEkipo(e);
        s.dodajEkipo(r);
        s.dodajEkipo(b);
        s.dodajEkipo(c);
        s.dodajTekmo("Danska", 1, "Nizozemska", 0);
        s.dodajTekmo("Nemčija", 1, "Portugalska", 0);


        System.out.println(s);*/

        // preberiMeritve("meritve1.txt");

        String[] t1 = {"miza", "stol", "sonce", "oblak", "dež", "kolo"};
        String[] t2 = {"drži", "ima", "nosi", "pije", "riše" };
        String[] t3 = {"lopato", "bolezen", "vročino", "smrad", "užitek"};

        genStavke(t1, t2, t3);
    }
}
