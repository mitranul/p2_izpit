public class izpiti03 {

    static void poisciUjemanje(String prvi, String drugi){
        String najdaljsi = "";
        int i = drugi.length();
        int j = 0;

        while(j < prvi.length() && j < i){
            while(i > 0){
                String substirng = prvi.substring(j, i);
                if(drugi.contains(substirng)) {
                    if(najdaljsi.length()<substirng.length())
                        najdaljsi=substirng;
                    break;
                }
                i--;
            }
            i = drugi.length();
            j++;
        }
        System.out.println(najdaljsi);
    }

    public static void main(String[] args) {
        poisciUjemanje(args[0], args[1]);
    }
}
