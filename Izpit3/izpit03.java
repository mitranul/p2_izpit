import java.util.ArrayList;
import java.util.Scanner;

class fooException extends Exception {
    @Override
    public String getMessage() {
        return "EMSO NI VELJAVEN!";
    }
}

class ArrayListPlus extends ArrayList<String> {
    @Override
    public String set(int index, String element) {
        try{
            super.set(index,element);
        }catch (IndexOutOfBoundsException IOBE) {
            for(int i=size();i<=index;i++)
                super.add("");
        }
        return super.set(index, element);
    }

    @Override
    public String toString() {
        String ret="";
        for(int i=0; i<super.size(); i++)
            ret+=super.get(i)+";";
        return ret.substring(0,ret.length()-1);
    }

    public ArrayListPlus(String seznam) {
        for(String e : seznam.split(";")) {
            super.add(e);
        }
    }
    public ArrayListPlus() {}
}

public class izpit03 {

    static void preveriEmso(String emso) throws fooException {
        int velikostEmsa = emso.length();
        if(velikostEmsa < 13) throw new fooException();

        for(int i = 0; i < velikostEmsa; i++) {
            if ((emso.charAt(i) >= 'A' && emso.charAt(i) <= 'Z') || (emso.charAt(i) >= 'a' && emso.charAt(i) <= 'z'))
                throw new fooException();

            if(i == 4 && emso.charAt(i) == '0' || emso.charAt(i) == '9')
                throw new fooException();

            if((i == 7 || i == 8) && (emso.charAt(i) == '5' && emso.charAt(i) == '0'))
                throw new fooException();
        }

        int dan = Integer.parseInt(emso.substring(0,2));
        int mesec = Integer.parseInt(emso.substring(2,4));
        int leto = Integer.parseInt(emso.substring(4, 6));

        System.out.println(dan + " " + mesec + " " + leto);
        int preostaneM = 12 - mesec;
        int preostaneD = preostaneM * 31 - preostaneM;

        System.out.println("Ko je bilo novo leto, je bila oseba stara: " + preostaneD);

    }

    static void razlicnaStevila(String imeDatoteke) {
        try {
            java.util.Scanner sc = new java.util.Scanner(new java.io.File(imeDatoteke));
            java.util.TreeSet<Integer> mnozica = new java.util.TreeSet<>();
            int steviloVseh = 0;
            float vsota = 0;

            while(sc.hasNextInt()) {
                int stevilo = sc.nextInt();
                mnozica.add(stevilo);
                steviloVseh++;
            }
            int velikostMnozice = mnozica.size();

            for(int stevilo : mnozica)
                vsota += stevilo;

            float pvp = vsota / velikostMnozice;

            System.out.printf("%d, %d, %.2f", steviloVseh, velikostMnozice, pvp);

        }catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) throws fooException {
        ArrayListPlus alp1 = new ArrayListPlus();
        alp1.set(3, "3");
        alp1.set(4, "4");
        alp1.set(1, "1");
        System.out.println(alp1); // ;1;;3;4

        ArrayListPlus alp2 = new ArrayListPlus(";b;c;;;;g");
        System.out.println(alp2); //;b;c;;;;g

        ArrayListPlus alp3 = new ArrayListPlus(";b;c;;;;g");
        alp3.set(0, "a");
        alp3.set(4, "e");
        System.out.println(alp3); // a;b;c;;e;;g
    }
}
