import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class Izpit2 {

    static void vlak() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Vpisi k: "); int k = sc.nextInt();
        System.out.println("Vpisi m: "); int m = sc.nextInt();
        System.out.println("Vpisi h: "); int h = sc.nextInt();

        int startMin = 5 * 60;
        int endMin = 24 * 60;
        int naslednji = 0;

    }

    static void urska(String datoteka1, String datoteka2) {
        try {
            Scanner sc = new Scanner(new File(datoteka1));
            String[] prva = sc.nextLine().split(" ");
            String format = prva[0];
            int velikostX = Integer.parseInt(prva[1]);
            int velikostY = Integer.parseInt(prva[3]);

            PrintWriter writer = new PrintWriter(datoteka2, "UTF-8");
            writer.println("G " + velikostX + " x " + velikostY);

            for(int piksel = 0; piksel < velikostX * velikostY; piksel++) {
                int r = sc.nextInt();
                int g = sc.nextInt();
                int b = sc.nextInt();

                int grey = (r + g + b) / 3;
                writer.print(grey + " ");
            }
            writer.close();
            sc.close();
        }catch(Exception e) {}
    }

    static void gui2() {
        JFrame frame = new JFrame("Obracanje");
        frame.setSize(500, 500);
        frame.setLocation(500, 500);
        frame.setResizable(false);

        JPanel panel = new JPanel();
        frame.add(panel);
        frame.setLayout(new FlowLayout(FlowLayout.LEFT));

        JTextField vnos = new JTextField(10);
        panel.add(vnos);

        JButton button = new JButton("Obrni");
        panel.add(button);

        JTextField izpis = new JTextField(10);
        panel.add(izpis);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                izpis.setText(obrni(vnos.getText()));
            }
        });

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    static String obrni(String niz) {
        if(niz.isEmpty())
            return "";
        return niz.charAt(niz.length() -  1) + obrni(niz.substring(0, niz.length() - 1));
    }

    public static void main(String[] args) {
        gui2();
    }
}
